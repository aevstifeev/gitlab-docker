ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG MAILROOM_VERSION
ARG GITLAB_USER=git

LABEL source="https://github.com/tpitale/mail_room/" \
      name="GitLab Mailroom" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${MAILROOM_VERSION} \
      release=${MAILROOM_VERSION} \
      summary="A configuration based process that will idle on IMAP connections and execute a delivery method when a new message is received." \
      description="A configuration based process that will idle on IMAP connections and execute a delivery method when a new message is received."

ADD gitlab-mailroom.tar.gz /

COPY scripts/ /scripts/

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf --disableplugin=subscription-manager --nogpgcheck install -yb --nodocs procps libicu tzdata \
    && adduser -m ${GITLAB_USER}

USER ${GITLAB_USER}:${GITLAB_USER}

CMD /usr/bin/mail_room -c /var/opt/gitlab/mail_room.yml

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
