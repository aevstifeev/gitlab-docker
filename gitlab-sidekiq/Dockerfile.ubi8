ARG RAILS_IMAGE=

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git

LABEL source="https://github.com/mperham/sidekiq" \
      name="GitLab Sidekiq" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Geo Log Cursor daemon." \
      description="Geo Log Cursor daemon."

ENV SIDEKIQ_CONCURRENCY=25
ENV SIDEKIQ_TIMEOUT=4

COPY scripts/ /scripts/

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf --disableplugin=subscription-manager --nogpgcheck install -yb --nodocs procps openssh-clients \
    && rm /usr/libexec/openssh/ssh-keysign \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} /scripts

USER ${GITLAB_USER}:${GITLAB_USER}

CMD /scripts/process-wrapper

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
